﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using TwainDotNet;
using TwainDotNet.Wpf;
using TwainDotNet.Win32;
using System.Windows.Interop;
using GoogleCloudStorage;
using System.Configuration;
using System.Windows;
using System.Reflection;
using System.Threading;
using System.Windows.Threading;

namespace TwainCloud
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Twain _twain;
        private ScanSettings _settings;
        private Bitmap _resultImage;
        private int _indexPage;
        private string _initialPath;
        

        private List<DocumentImage> imageList = new List<DocumentImage>();
        private FileManager oFileManager = new FileManager();

        public MainWindow()
        {
            if (! FirstInstance)
            {
                this.Close();
            }

            InitializeComponent();
            oFileManager.ValidateFolders();
            // Inicializa el proceso en segundo plano
            oFileManager.CleanFolders();

            ZoomViewbox.Height = 480;
            ZoomViewbox.Width = 720;
            this._indexPage = 0;
            this._initialPath = ConfigurationManager.AppSettings["pathPDFDocs"].ToString();

            Loaded += delegate
            {
                _twain = new Twain(new WpfWindowMessageHook(this));
                _twain = new Twain(new WpfWindowMessageHook(this));
                _twain.TransferImage += delegate (Object sender, TransferImageEventArgs args)
                {
                    if (args.Image != null)
                    {
                        DocumentImage oDocumentImage = new DocumentImage
                        {
                            ImageBmp = args.Image
                        };
                        imageList.Add(oDocumentImage);
                        _resultImage = args.Image;
                        IntPtr hbitmap = new Bitmap(args.Image).GetHbitmap();
                        MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(
                                hbitmap,
                                IntPtr.Zero,
                                Int32Rect.Empty,
                                BitmapSizeOptions.FromEmptyOptions());
                        Gdi32Native.DeleteObject(hbitmap);
                        _indexPage++;
                    }
                };
                _twain.ScanningComplete += delegate
                {
                    IsEnabled = true;
                };

                var sourceList = _twain.SourceNames;
                ManualSource.ItemsSource = sourceList;

                if (sourceList != null && sourceList.Count > 0)
                    ManualSource.SelectedItem = sourceList[0];
            };
        }
        private void OnSelectSourceButtonClick(object sender, RoutedEventArgs e)
        {
            try
            {
                _twain.SelectSource();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        private void ScanButton_Click(object sender, RoutedEventArgs e)
        {
            IsEnabled = false;

            imageList.Clear();
            _indexPage = 0;

            _settings = new ScanSettings
            {
                UseDocumentFeeder = UseAdfCheckBox.IsChecked,
                ShowTwainUI = (bool) UseUIScanner.IsChecked,
                ShowProgressIndicatorUI = true,
                UseDuplex = UseDuplexCheckBox.IsChecked,
                Resolution = ResolutionSettings.ColourPhotocopier,
                Area = null,
                ShouldTransferAllPages = true,
                Rotation = new RotationSettings
                {
                    AutomaticRotate = false,
                    AutomaticBorderDetection = false
                }
            };

            try
            {
                _twain.SelectSource(ManualSource.SelectedItem.ToString());
                _twain.StartScanning(_settings);
            }
            catch (TwainException ex)
            {
                MessageBox.Show(ex.Message);
            }

            IsEnabled = true;
        }

        /// <summary>
        /// Procesa los archivos escaneados para almacenarlos en Google Cloud Storage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSaveButtonClick(object sender, RoutedEventArgs e)
        {
            if (_resultImage != null)
            {
                string sFileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".pdf";

                // Genera los pdf's a partir de la Lista de Imágenes escaneadas
                this.SaveImagesFromScanner();
                this.CreatePDFFromImg(sFileName);
                string PDFPath = oFileManager.PDFSource + sFileName; 

                this.OnWorkerMethodStart(PDFPath);
            }
        }

        private void LoadImageButton_Click(object sender, RoutedEventArgs e)
        {
            
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Archivos PDF|*.pdf";
            ofd.InitialDirectory = this._initialPath;
            if (ofd.ShowDialog() == true)
            {

                string PDFPath = ofd.FileName;
                /**
                Image img = Image.FromFile(fileName);
                this.activarElementosForm(img);
            */

                this.OnWorkerMethodStart(PDFPath);
            }
        }

        private void OnWorkerMethodStart(string PDFPath)
        {
            Thread backgroundThread = new Thread(
                new ThreadStart(() =>
                {
                    // Set the flag that indicates if a process is currently running

                    // Set the dialog to operate in indeterminate mode
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                    {
                        this.pb.IsIndeterminate = true; // Do all the ui thread updates here
                    }));

                    // Pause the thread for five seconds
                    this.UploadPDF(PDFPath);


                    // Show a dialog box that confirms the process has completed
                    MessageBox.Show("Archivo enviado. \n Enlace copiado al portapapeles.");
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                    {
                        this.pb.IsIndeterminate = false; // Do all the ui thread updates here
                    }));

                    // Reset the flag that indicates if a process is currently running

                }
            ));
            backgroundThread.Start();
        }

        /// <summary>
        /// Llama el método para exportar las imagenes a JPG
        /// </summary>
        private void SaveImagesFromScanner()
        {
            oFileManager.SaveImages(imageList);
        }

        /// <summary>
        /// Llama método para crear PDF de imágenes escaneadas
        /// </summary>
        private void CreatePDFFromImg(string sFileName)
        {
            oFileManager.SaveImagesAsPDF(imageList, sFileName);
        }

        /// <summary>
        /// Ejecuta las instrucciones para subir el PDF a la nube
        /// </summary>
        private void UploadPDF(string PDFPath)
        {
            Storage storage = new Storage();
            var uri = storage.UploadFile(PDFPath, type: "application/pdf");
            if ( ! String.IsNullOrEmpty(uri))
            {
                this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(delegate ()
                {
                    TxtUri.Text = uri;
                    Clipboard.SetText(uri);
                }));
                

            }

        }

        /// <summary>
        /// Altera el zoom del contendor de la imagen de acuerdo al scroll del raton
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindow_OnMouseWheel(object sender, MouseWheelEventArgs e)
        {
            UpdateViewBox((e.Delta > 0) ? 25 : -25);
        }

        /// <summary>
        /// Altera el zoom de la imagen
        /// </summary>
        /// <param name="newValue"></param>
        private void UpdateViewBox(int newValue)
        {
            if (newValue > 0)
            {
                ZoomViewbox.Width += newValue;
                ZoomViewbox.Height += newValue;
            } else
            {
                if ((ZoomViewbox.Width >= 200) && ZoomViewbox.Height >= 200)
                {
                    ZoomViewbox.Width += newValue;
                    ZoomViewbox.Height += newValue;
                }
            }
            
        }

        /// <summary>
        /// Aleja el zoom del contenedor de la imagen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomOut_Click(object sender, RoutedEventArgs e)
        {
            UpdateViewBox(-25);
        }

        /// <summary>
        /// Aumenta el zoom del contenedor de la imagen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ZoomIn_Click(object sender, RoutedEventArgs e)
        {
            UpdateViewBox(25);
        }

        private void NextPage_Click(object sender, RoutedEventArgs e)
        {

            int total = imageList.Count;
            if (total > 1 && _indexPage < total)
            {
                IntPtr hbitmap = new Bitmap(imageList[_indexPage++].ImageBmp).GetHbitmap();
                MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(
                        hbitmap,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                Gdi32Native.DeleteObject(hbitmap);
            }
        }

        private void PreviousPage_Click(object sender, RoutedEventArgs e)
        {
            int total = imageList.Count;
            if (total > 1 && _indexPage > 1 && _indexPage <= total)
            {
                IntPtr hbitmap = new Bitmap(imageList[--_indexPage - 1].ImageBmp).GetHbitmap();
                MainImage.Source = Imaging.CreateBitmapSourceFromHBitmap(
                        hbitmap,
                        IntPtr.Zero,
                        Int32Rect.Empty,
                        BitmapSizeOptions.FromEmptyOptions());
                Gdi32Native.DeleteObject(hbitmap);
            }
        }

        private static bool FirstInstance
        {
            get
            {
                bool created;
                string name = Assembly.GetEntryAssembly().FullName;
                // created will be True if the current thread creates and owns the mutex.
                // Otherwise created will be False if a previous instance already exists.
                Mutex mutex = new Mutex(true, name, out created);
                return created;
            }
        }
    }
}
