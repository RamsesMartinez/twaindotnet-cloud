﻿using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows;

namespace TwainCloud
{
    class FileManager
    {
        private readonly string _ImgSource = @"img\";
        private readonly string _PDFSource = @"pdf\";

        public string PDFSource { get { return this._PDFSource; } }

        /// <summary>
        /// Función que permite almacenar todas las imagenes escaneadas en un PDF único
        /// </summary>
        /// <param name="sFileName"></param>
        public void SaveImagesAsPDF(List<DocumentImage> documentImages, string sFileName)
        {
            int count = 0;
            PdfDocument doc = new PdfDocument();

            try
            {

                foreach (DocumentImage oDocImage in documentImages)
                {
                    string file = this._ImgSource + oDocImage.FileName;
                    if (File.Exists(file))
                    {
                        doc.Pages.Add(new PdfPage());
                        XGraphics xgr = XGraphics.FromPdfPage(doc.Pages[count++]);
                        XImage img = XImage.FromFile(Path.GetFullPath(file));
                        xgr.DrawImage(img, 0, 0);
                    }
                   
                }

                if (count > 0) {
                    doc.Save(@"pdf/" + sFileName);
                } else
                {
                    throw new Exception("No hay imagenes para crear el PDF");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error");
            }
            finally
            {
                doc.Close();
                GC.Collect();
            }
        }

        public void ValidateFolders()
        {
            if (! Directory.Exists(this._ImgSource))
            {
                Directory.CreateDirectory(this._ImgSource);
            }
            if (! Directory.Exists(this._PDFSource))
            {
                Directory.CreateDirectory(this._PDFSource);
            }
        }

        public void CleanFolders()
        {
            try
            {
                foreach (string file in Directory.GetFiles(this._PDFSource))
                {
                    File.Delete(file);
                }

                foreach (string file in Directory.GetFiles(this._ImgSource))
                {
                    File.Delete(file);
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Crea archivos JPG a partir de objetos Bitmap contenidos en un objeto List
        /// </summary>
        /// <param name="imagesList"></param>
        public void SaveImages(List<DocumentImage> imagesList)
        {
            try
            {
                foreach (DocumentImage documentImg in imagesList)
                {
                    documentImg.SaveImg(_ImgSource);
                }
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            } finally
            {
                GC.Collect();

                GC.WaitForPendingFinalizers();
            }
        }
    }
}
