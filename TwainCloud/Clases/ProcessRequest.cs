﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace TwainCloud
{
    class ProcessRequest
    {
        
        public ProcessRequest()
        {
        }

        public string GetData()
        {
            string bResponse = string.Empty;
            try
            {
                
                // create a thread  
                Thread newWindowThread = new Thread(new ThreadStart(() =>
                {
                    // create and show the window
                    var mainwindow = new MainWindow();
                    try
                    {
                        mainwindow.ShowDialog();
                        
                    }
                    catch (Exception ex) { }
                    
                    // start the Dispatcher processing  
                    System.Windows.Threading.Dispatcher.Run();

                }));

                // set the apartment state  
                newWindowThread.SetApartmentState(ApartmentState.STA);

                // make the thread a background thread  
                newWindowThread.IsBackground = true;

                // start the thread  
                newWindowThread.Start();
                newWindowThread.Join();
                Console.WriteLine("PASO");
                
            }
            catch (Exception exp1)
            {
                bResponse = "ERROR-Error al llamar dispositivo: " + exp1.Message;
            }
            return bResponse;
        }
    }
}
