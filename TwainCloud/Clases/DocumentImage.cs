﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwainCloud
{
    /// <summary>
    /// Clase que permite almacenar informacion de las imágenes escaneadas
    /// </summary>
    class DocumentImage
    {
        public DocumentImage()
        {
            this.FileName = DateTime.Now.ToString("yyyyMMddHHmmssffff") + ".jpg";
        }
        public string FileName { get; set; }
        public Bitmap ImageBmp { get; internal set; }

        internal void SaveImg(string path)
        {
            this.ImageBmp.Save(path + this.FileName);
        }
    }
}
