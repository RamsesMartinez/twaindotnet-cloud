﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace TwainCloud
{
    /// <summary>
    /// Lógica de interacción para App.xaml
    /// </summary>
    public partial class App : System.Windows.Application
    {
        #region variables
        public static string _sOwnLog = @Environment.CurrentDirectory + @"\TwainCloud.log";
        public static NotifyIcon notifyIcon = new NotifyIcon();

        //Variable para saber si ya se registro en el inicio la aplicación
        string _sRegistroInicio = string.Empty;

        private string _sShowUnregister = string.Empty;
        private string _sShowExit = string.Empty;

        MenuItem aboutMenuItem;
        MenuItem exitMenuItem;
        #endregion

        [STAThread]
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            
            try {
                Log(_sOwnLog, String.Format("{0} -:’{1}’", this.ToString(), "--------------------"), false);
                Log(_sOwnLog, String.Format("{0} -:’{1}’", this.ToString(), "Iniciando FYG Twain Cloud"), false);
                Log(_sOwnLog, String.Format("{0} -:’{1}’", this.ToString(), "Iniciando FYG Twain Cloud"), false);
                
                //Verifico si la aplicación ya se encuentra dada de alta en el Registro de Inicio
                Log(_sOwnLog, String.Format("{0} -:’{1}’", this.ToString(), "Verificando si la aplicación está registrada en el Inicio"), false);
                _sRegistroInicio = this.ReadKeyConfiguration("REGISTRADO");

                if (!_sRegistroInicio.Equals("1"))
                {
                    try
                    {
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "La Aplicación no está registrada en el Inicio, registrando..."), false);
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Actualizando archivo de configuración..."), false);
                        SaveKeyConfiguration("REGISTRADO", "1"); 
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Archivo de configuración actualizado"), false);
                        RegisterInStartup(true);
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "La Aplicación fue registrada en el Inicio Exitosamente"), false);
                    }
                    catch (Exception excpRegistraInicio)
                    {
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Error al Registrar la Aplicación en el Inicio: " + excpRegistraInicio.Message), false);
                        Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Ejecute la Aplicación para volver a intentarlo"), false);
                    }
                }

                HttpServer httpServer = new HttpServer(8080, Routes.GET);

                Thread thread = new Thread(new ThreadStart(httpServer.Listen));
                thread.Start();

                // Menú de ícono en barra de tareas
                aboutMenuItem = new MenuItem("Quitar Manejador de periféricos del Inicio", new EventHandler(ShowInfo));
                exitMenuItem = new MenuItem("Salir", new EventHandler(Shutdown));

                this._sShowUnregister = this.ReadKeyConfiguration("SHOW_UNREGISTER");
                this._sShowExit = this.ReadKeyConfiguration("SHOW_EXIT");

                // Verifica menús
                if (_sShowUnregister.Equals("1") && _sShowExit.Equals("1"))
                {
                    notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] 
                    {
                        aboutMenuItem, exitMenuItem
                    });
                }
                else if (_sShowExit.Equals("1"))
                {
                    notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] {
                        exitMenuItem
                    });
                }
                else if (_sShowUnregister.Equals("1"))
                {
                    notifyIcon.ContextMenu = new ContextMenu(new MenuItem[] 
                    {
                        aboutMenuItem
                    });
                }
                else if (_sShowUnregister.Equals("") || _sShowExit.Equals(""))
                {
                    this.SaveKeyConfiguration("SHOW_UNREGISTER", "0");
                    this.SaveKeyConfiguration("SHOW_EXIT", "0");
                }

                // Parámetros para la aplicación de contexto de Windows
                notifyIcon.Text = String.Format(":: FYG Twain Cloud {0}© ::", DateTime.Today.Year.ToString());
                notifyIcon.Icon = TwainCloud.Properties.Resources.favicon;
                notifyIcon.DoubleClick += new EventHandler(ShowMessage); //ejemplo de evento por si se necesita
                notifyIcon.Visible = true;
                //Notifico al usuario que se está ejecutando el componente
                notifyIcon.ShowBalloonTip(3000, "FYG Solutions ©", "Servidor local iniciado", ToolTipIcon.Info);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        #region log de la aplicación
        //Generador de Log
        public static void Log(string sPathName, string sMsg, bool MostrarMsgBox = true)
        {
            ErrorLog(sPathName, sMsg);
            if ((MostrarMsgBox == true))
            {
                //MessageBox.Show(sMsg, "Cuadro de Mensajes", MessageBoxButtons.OK);
            }
        }
        public static void ErrorLog(string sPathName, string sErrMsg)
        {
            try
            {
                string sYear = DateTime.Now.Year.ToString();
                string sMonth = DateTime.Now.Month.ToString();
                string sDay = DateTime.Now.Day.ToString();
                string sErrorTime = sYear + sMonth + sDay;
                string sLogFormat = DateTime.Now.ToShortDateString().ToString() + " " + DateTime.Now.ToLongTimeString().ToString() + " ==> ";
                StreamWriter sw = new StreamWriter(sPathName.Replace(".log", "." + sErrorTime + ".log"), true);
                sw.WriteLine(sLogFormat + sErrMsg);
                sw.Flush();
                sw.Close();
            }
            catch
            {
                FileInfo fi = new FileInfo(sPathName);
                Directory.CreateDirectory(fi.DirectoryName);
                ErrorLog(sPathName, sErrMsg);
            }
        }
        #endregion

        #region RegisterInStartup
        private void RegisterInStartup(bool isChecked)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey
                    ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            if (isChecked)
            {
                registryKey.SetValue("twaindotnet-cloud.exe", System.Windows.Forms.Application.ExecutablePath);
            }//if
            else
            {
                registryKey.DeleteValue("twaindotnet-cloud.exe");
            }//else
        }
        #endregion

        #region Utils (leerValorConfiguracion, guardarValorConfiguracion)
        public string ReadKeyConfiguration(string key)
        {
            try
            {
                return ConfigurationManager.AppSettings[key].ToString();
            }//try
            catch
            {
                return "";
            }//catch
        }//leerValorConfiguracion


        public void SaveKeyConfiguration(string key, string value)
        {
            try
            {
                //La línea siguiente no funcionará bien en tiempo de diseño 
                //pues VC# usa el fichero xxx.vshost.config en la depuración 
                //Configuration config = 
                //    ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None); 
                //sí pues la cambiamos por: 
                Configuration ficheroConfXML = ConfigurationManager.OpenExeConfiguration(System.Windows.Forms.Application.ExecutablePath);

                //eliminamos la clave actual (si existe), si no la eliminamos 
                //los valores se irán acumulando separados por coma 
                ficheroConfXML.AppSettings.Settings.Remove(key);

                //asignamos el valor en la clave indicada 
                ficheroConfXML.AppSettings.Settings.Add(key, value);

                //guardamos los cambios definitivamente en el fichero de configuración 
                ficheroConfXML.Save(ConfigurationSaveMode.Modified);
            }//try
            catch
            {
                /* MessageBox.Show("Error al guardar valor de configuración: " + 
                     System.Environment.NewLine + System.Environment.NewLine + 
                     ex.GetType().ToString() + System.Environment.NewLine + 
                     ex.Message, "Error", 
                     MessageBoxButtons.OK, MessageBoxIcon.Error);*/
            }//catch
        }//guardarValorConfiguracion
        #endregion

        #region ShowInfo (Muestra acerca de)
        void ShowInfo(object sender, EventArgs e)
        {
            try
            {
                Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Clic Quitar FYG Twain Cloud de Inicio de Windows"), false);
                this.SaveKeyConfiguration("REGISTRADO", "0");
                RegisterInStartup(false);
                Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "FYG Twain Cloud Quitado de Inicio de Windows"), false);
                notifyIcon.ShowBalloonTip(3000, "FYG Solutions ©", "FYG Twain Cloud Quitado de Inicio de Windows", ToolTipIcon.Info);
            }
            catch (Exception excpRegistraInicio)
            {
                Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Error al Quitar la Aplicación en el Inicio: " + excpRegistraInicio.Message), false);
                notifyIcon.ShowBalloonTip(3000, "FYG Solutions ©", "Error al Quitar la Aplicación en el Inicio: Revise Log", ToolTipIcon.Info);
            }//catch
        }//ShowInfo
        #endregion

        #region showmessage
        void ShowMessage(object sender, EventArgs e)
        {
            System.Windows.MessageBox.Show(String.Format(":: FYG Twain Cloud {0}© ::", DateTime.Today.Year.ToString()), "FYG Solutions ©");
            Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Clic Show Message: FYG Twain Cloud", "FYG Solutions ©"), false);
        }
        #endregion

        #region exit
        void Shutdown(object sender, EventArgs e)
        {
            notifyIcon.Visible = false;
            Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Clic Exit"), false);
            Log(_sOwnLog, String.Format("FYG Twain Cloud -:’{0}’", "Fin de TwainCloud"), false);
            System.Windows.Application.Current.Shutdown();

        }
        #endregion
    }
}
